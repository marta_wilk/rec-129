package com.intelicom.contacts.service;

import java.util.List;

import com.intelicom.contacts.form.Contact;

public interface ContactService {
	
	public void addContact(Contact contact);
	public List<Contact> listContact();
	public void removeContact(Integer id);
}
