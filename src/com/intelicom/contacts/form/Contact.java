package com.intelicom.contacts.form;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CONTACTS")

public class Contact {
	
	@Id
	@Column(name="ID")
	@GeneratedValue
	private Integer id;
	
	@Column(name="FIRST NAME")
	private String firstname;

	@Column(name="LAST NAME")
	private String lastname;

	@Column(name="EMAIL")
	private String email;
	
	@Column(name="PHONE NUMBER")
	private String telephone;

	@Column(name="ADDRESS")
	private String address;

	@Column(name="CITY")
	private String city;

	@Column(name="ZIP")
	private Integer zip;

	@Column(name="IS FRIEND")
	private String isFriend;

	public String getEmail() {
		return email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getFirstname() {
		return firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getZip() {
		return zip;
	}

	public void setZip(Integer zip) {
		this.zip = zip;
	}

	public String getIsfriend() {
		return isFriend;
	}

	public void setIsfriend(String isFriend) {
		this.isFriend = isFriend;
	}
}
