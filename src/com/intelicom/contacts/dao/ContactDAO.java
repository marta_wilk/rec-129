package com.intelicom.contacts.dao;

import java.util.List;

import com.intelicom.contacts.form.Contact;

public interface ContactDAO {
	
	public void addContact(Contact contact);
	public List<Contact> listContact();
	public void removeContact(Integer id);
}
